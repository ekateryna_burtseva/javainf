package t3;

import java.util.Scanner;

public class HappyTickets {

	public static void main(String[] args) {
		int i1, i2;
		int count = 0;
		for (int a = 0; a <= 999999; a++) {
			i1 = a / 100000 + a / 10000 % 10 + a / 1000 % 10;
			i2 = a / 100 % 10 + a / 10 % 10 + a % 10;
			if (i1 == i2) {
				count += 1;
			}
		}
		System.out.println(count);
	}
}
