/*��� ������ A ������� N (N � ������ �����). ������� ��� �������� �
������� �������� � ������� ����������� �������: A2, A4, A6, ..., AN.
�������� �������� �� ������������.
*/

package t3;

import java.util.Scanner;

public class Task5 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value n");
		int n = in.nextInt();
		int[] a = new int[n];
		for (int i = 0; i < a.length; i++) {
			a[i] = (int) (Math.random() * 10);
		}
		for (int i = 0; i < n; i = i + 2) {
			System.out.println(a[i]);
		}

	}

}
