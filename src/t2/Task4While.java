package t2;

import java.util.Scanner;

public class Task4While {

	public static void main(String[] args) {
		Scanner in3 = new Scanner(System.in);
		System.out.println("Enter value n");
		int n = in3.nextInt();
		int c = 0;
		while (n > 3 && n > Math.pow(3, c)) {
			c++;
		}
		if (n == Math.pow(3, c)) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}

	}
}
