package t2;

import java.util.Scanner;

public class Task1While {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value a");
		int a = in.nextInt();
		System.out.println("Enter value b");
		int b = in.nextInt();
		int count = 0;
		while (a <= b) {
			System.out.println(a++);
			count = count + 1;
		}
		System.out.println(count);

	}

}
