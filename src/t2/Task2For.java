package t2;

import java.util.Scanner;

public class Task2For {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value n");
		int n = in.nextInt();
		double res=1;
		for (int i = 1; i <= n; i++) {
			res=res*i;
		}
		System.out.println(res);

	}

}
