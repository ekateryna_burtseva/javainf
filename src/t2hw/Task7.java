/*
 * ���� ����� �����, ������� � ��������� 1�999. ������� ���
������-�������� ���� ������� ���������� �����, ���������
����������� ����� � �. �.
 */

package t2hw;

import java.util.Scanner;

public class Task7 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value a");
		int a = in.nextInt();

		if (a % 2 == 0 && a <= 99 && a >= 10) {
			System.out.println("������ ���������� �����");
		} else if (a % 2 > 0 && a <= 99 && a >= 10) {
			System.out.println("�������� ���������� �����");
		}

		if (a % 2 == 0 && a >= 1 && a <= 9) {
			System.out.println("������ ����������� �����");
		} else if (a % 2 > 0 && a >= 1 && a <= 9) {
			System.out.println("�������� ����������� �����");
		}

		if (a % 2 == 0 && a >= 100 && a <= 999) {
			System.out.println("������ ����������� �����");
		} else if (a % 2 > 0 && a >= 100 && a <= 999) {
			System.out.println("�������� ����������� �����");
		}
	}
}
