/*
 * ���� ��� ����� ����� A � B (A < B). ������� � ������� �������� ��� ����� �����, 
 * ������������� ����� A � B (�� ������� ����� A � B), � ����� ���������� N ���� �����.
 */
package t2hw;

import java.util.Scanner;

public class Task2While {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a from keyboard");
		int a = in.nextInt();
		System.out.println("Enter b from keyboard");
		int b = in.nextInt();
		int count = 0;
		while (a + 1 < b) {
			System.out.println(--b);
			count = count + 1;
		}
		System.out.println(count);

	}

}
