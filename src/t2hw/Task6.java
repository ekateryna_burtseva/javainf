/*
 * ��� ����� ���� (������������� ����� �����). ���������� ����������
���� � ���� ����, ��������, ��� ������� ��� ����������� 365 ����, �
���������� � 366 ����. ���������� ��������� ���, ��������� �� 4, ��
����������� ��� �����, ������� ������� �� 100 � �� ������� �� 400
(��������, ���� 300, 1300 � 1900 �� �������� �����������, � 1200 � 2000
� ��������).
 */
package t2hw;

import java.util.Scanner;


public class Task6 {

	public static void main(String[] args) {
		Scanner in3 = new Scanner(System.in);
		System.out.println("Enter year from keyboard");
		int year = in3.nextInt();
		int count = 365;
		if (year % 100 == 0 && year % 400 > 0 || year % 4 > 0) {
			System.out.println(count);
		} else if (year % 4 == 0) {
			System.out.println(count + 1);
		}

	}

}
