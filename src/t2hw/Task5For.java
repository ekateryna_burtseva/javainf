/*Даны положительные числа A, B, C. На прямоугольнике размера A×B размещено
максимально возможное количество квадратов со стороной C (без наложений). Найти
количество квадратов, размещенных на прямоугольнике. Операции умножения и деления не
использовать.
*/
package t2hw;

import java.util.Scanner;

public class Task5For {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value a");
		int a = in.nextInt();
		System.out.println("Enter value b");
		int b = in.nextInt();
		System.out.println("Enter value c");
		int c = in.nextInt();
		int count = 0;
		int k = 0;
		int k2 = 0;
		while (a >= c) {
			a -= c;
			k++; // количество квадратов по стороне A
		}
		while (b >= c) {
			b -= c;
			k2++; // количество квадратов по стороне В
		}
		for (int i = 0; i < k; i++) {
			count += k2;
		}
		System.out.println(count);

	}

}
