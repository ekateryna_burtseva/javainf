/*
 * ���� ��� ������� A � B, �������� ������� �����������
�� �����������. ���������� ��� ������� ���, �����
�������������� ������ C ������� ������������� ��
�����������.

 */
package t3hw;

import java.util.Scanner;

public class Task9 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value n");
		int n = in.nextInt();
		int[] a = { 2, 4, 5, 6 };
		int[] b = { 4, 8, 9, 0 };
		
		int[] c = new int[n];
		for (int i = 0; i < n / 2; i++) {
			if (a[i] > b[i]) {
				c[i + i] = b[i];
				c[i + i + 1] = a[i];
			} else {
				c[i + i] = a[i];
				c[i + i + 1] = b[i];

			}
		}

		for (int i = 0; i < n - 1; i++) {
			int t = c[i];
			if (c[i] > c[i + 1]) {
				c[i] = c[i + 1];
				c[i + 1] = t;
			}

			System.out.println(c[i]);

		}

	}
}
