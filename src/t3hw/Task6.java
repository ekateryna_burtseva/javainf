/*��� ������ ��������� ����� ����� ������� N. ���������, ���������� ��
� ��� ������������� � ������������� �����. ���� ����������, �� �������
0, ���� ���, �� ������� ���������� ����� ������� ��������, �����������
��������������.
*/
package t3hw;

import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value n");
		int n = in.nextInt();
		int[] a = { 2, -4, 5, -7, 8 };
		for (int i = 1; i < n; i++) {
			if (a[i - 1] > 0 && a[i] > 0 || a[i - 1] < 0 && a[i] < 0) {
				System.out.println(i);
				break;
			}
			if (a[i - 1] > 0 && a[i] < 0 || a[i - 1] < 0 && a[i] > 0) {
				System.out.println(0);
				break;
			}
		}
	}
}


