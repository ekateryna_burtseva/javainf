/*
 * ��� ������������� ������ ������� N. ������� ������� ���
������������ � ������ ������� ������ ����� � ������� ����������� ��
��������, � ����� � ��� �������� ����� � ������� �������� �� ��������.
����� ������� ���������� ������ � �������� ������ �������.
 */
package t3hw;

import java.util.Scanner;

public class Task4 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value n");
		int n = in.nextInt();
		int count1 = 0;
		int count2 = 0;
		int a[] = { 2, 5, 4, 3, 6};
		System.out.println("Array only with even numbers ");
		for (int i = 0; i < n; i++) {

			if (a[i] % 2 == 0) {
				System.out.println(a[i]);
				count1 += 1;
			}
		}
		System.out.println("Even numbers count equals to " + count1);
		System.out.println("Array only with odd numbers ");
		for (int i = n - 1; i >= 0; i--) {
			if (a[i] % 2 > 0) {

				System.out.println(a[i]);
				count2 += 1;
			}
		}
		System.out.println("Odd numbers count equals to " + count2);
	}
}
