/*
 * Дана матрица размера M × N и целое число K (0 ≤ K ≤ M).
Удалить строку матрицы с номером K.
 */
package t4hw;

import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value m");
		int m= in.nextInt();
		System.out.println("Enter value n");
		int n= in.nextInt();
		System.out.println("Enter value k");
		int k=in.nextInt();
		int[][] a = new int[m][n];
		System.out.println("Old array");
		for(int i=0;i<m;i++){
			for (int j=0;j<n;j++){
				a[i][j]= (int)(Math.random()*10);
				System.out.print(a[i][j]+" ");
			}
			 System.out.println();
		}
		
		System.out.println("New array");
		for(int i=0;i<m;i++){
			for (int j=0;j<n;j++){
				if(i>=k-1){
					  a[i][j]=a[i+1][j];
				  }
                System.out.print(a[i][j]+" ");
                }
                System.out.println();
            }
    }
	}


