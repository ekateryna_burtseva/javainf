/*
 * Дана матрица размера M × N. Отсортировать ее так, чтоб
элементы выстроились в порядке возрастания от номера (0,0) к
номеру (M-1,N-1). За каждым последним элементом одной
строки идет первый элемент другой.
 */

package t4hw;

import java.util.Scanner;

public class Task7 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value m");
		int m = in.nextInt();
		System.out.println("Enter value n");
		int n = in.nextInt();
		int[][] a = new int[m][n];

		System.out.println("Old array");
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < n; ++j) {
				a[i][j] = (int) (Math.random() * 10);
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("New array");
		int tmp = 0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < a[i].length; j++) {
				for (int k = 0; k < m; k++) {
					for (n = 0; n < a[k].length; n++) {
						if (a[k][n] > a[i][j]) {
							tmp = a[k][n];
							a[k][n] = a[i][j];
							a[i][j] = tmp;
						}
					}
				}
			}
		}

		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < a[i].length; ++j) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
	}
}