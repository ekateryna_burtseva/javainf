/*
 * Дана матрица размера M × N и целые числа K1 и K2. Поменять
местами строки матрицы с номерами K1 и K2.
 */
package t4hw;

import java.util.Scanner;

public class Task5 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value m");
		int m = in.nextInt();
		System.out.println("Enter value n");
		int n = in.nextInt();
		int[][] a = new int[m][n];
		System.out.println("Enter value k1");
		int k1 = in.nextInt();
		System.out.println("Enter value k2");
		int k2 = in.nextInt();
		System.out.println("Old array ");
		int tmp;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				a[i][j] = (int) (Math.random() * 10);
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}

		k1--;
		k2--;
		System.out.println("New array ");
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (i == k1) {
					tmp = a[k1][j];
					a[k1][j] = a[k2][j];
					a[k2][j] = tmp;
				}
				System.out.print(a[i][j] + " ");

			}
			System.out.println();

		}
	}

}
